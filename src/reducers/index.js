import { combineReducers } from 'redux';
import loginUser from './loginUser';

// За какую часть отвечает наш module
// state.count
// Значение будет module который умеет управлять частью нашего store
export default combineReducers({
    loginUser
});