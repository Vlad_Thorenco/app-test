import {MAIL_USER, CHANGE_INPUT_VALUE} from '../constans/index';

const initialState = {
    activeUserMail: [],
    userName: '',
    userPassword: '',
    userMail: '',
};

export default (state = initialState, action) => {
    switch (action.type) {
        case MAIL_USER:
            return Object.assign({}, {
                ...state,
                activeUserMail: state.activeUserMail
            });
        case CHANGE_INPUT_VALUE:
            return Object.assign({}, {
                ...state,
                userName: state.userName
            });
        default:
            return state;
    }
}



