import React, {Component} from "react";
import './style.css';
import Menu from "../../components/Navigation/Menu";
import Header from "../../components/Header";

class Layout extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="Layout">
                <Header/>
                <Menu />
            </div>
        )
    };
}

export default Layout;

