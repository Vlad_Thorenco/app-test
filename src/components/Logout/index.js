import React, {Component} from "react";
import './style.css';

class Logout extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="Logout">
                <p>email: <span>vlad.thorenco18@gmail.com</span></p>
                <a href="" className="logout-user">logout</a>
            </div>
        )
    };
}

export default Logout;

