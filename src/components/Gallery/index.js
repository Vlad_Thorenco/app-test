import React, {Component} from "react";
import './style.css';
import ImageGallery from 'react-image-gallery';
import "../../../node_modules/react-image-gallery/styles/css/image-gallery.css";
import "../../../node_modules/react-image-gallery/styles/css/image-gallery-no-icon.css";

class Gallery extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const images = [
            {
                original: 'https://pulson.ru/wp-content/uploads/2014/01/wallpaper-1664888.jpg',
                thumbnail: 'https://pulson.ru/wp-content/uploads/2014/01/wallpaper-1664888.jpg'
            },
            {
                original: 'https://pulson.ru/wp-content/uploads/2014/01/wallpaper-2591311.jpg',
                thumbnail: 'https://pulson.ru/wp-content/uploads/2014/01/wallpaper-2591311.jpg'
            },
            {
                original: 'https://pulson.ru/wp-content/uploads/2014/01/wallpaper-2824374.jpg',
                thumbnail: 'https://pulson.ru/wp-content/uploads/2014/01/wallpaper-2824374.jpg'
            },
            {
                original: 'https://pulson.ru/wp-content/uploads/2014/01/wallpaper-2552971.jpg',
                thumbnail: 'https://pulson.ru/wp-content/uploads/2014/01/wallpaper-2552971.jpg'
            },
            {
                original: 'https://pulson.ru/wp-content/uploads/2014/01/wallpaper-3032568.jpg',
                thumbnail: 'https://pulson.ru/wp-content/uploads/2014/01/wallpaper-3032568.jpg'
            }
        ];

        return (
            <div>
                <ImageGallery items={images}/>
            </div>
        )
    };
}

export default Gallery;

