import React from 'react';
import {NavLink} from "react-router-dom";
import './style.css';

const MenuItem = (props) => {
    return (
        <li key={props.id}>
            <NavLink to={props.path} exact={props.exact}>
                {props.linkItem}
            </NavLink>
        </li>
    )
};
export default MenuItem