import React, {Component} from "react";
import './style.css';
import {Route} from "react-router-dom";
import MenuItem from "./MenuItem";
import Video from "../../Video";
import Gallery from "../../Gallery";
import Charts from "../../Charts";
import PopUpForm from '../../PopUp/index'

class Menu extends Component {
    static propTypes = {};

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const LinkItem = [
            {
                id: 0,
                label: 'Video',
                path: '/video',
                exact: true
            },
            {
                id: 1,
                label: 'Gallery',
                path: '/gallery',
                exact: false
            },
            {
                id: 2,
                label: 'PopUp',
                path: '/pop-up',
                exact: false
            },
            {
                id: 3,
                label: 'Charts',
                path: '/charts',
                exact: false
            },
        ];
        return (
            <div className="Wrapped-menu">
                <ul className="Menu">
                    {LinkItem.map((linkItem) => {
                        return <MenuItem linkItem={linkItem.label}
                                         key={linkItem.id}
                                         path={linkItem.path}
                                         exact={linkItem.exact}
                        />
                    })}
                </ul>
                { /* Route components */}
                <Route path="/video" exact component={Video}/>
                <Route path="/gallery" component={Gallery}/>
                <Route path="/pop-up" component={PopUpForm}/>
                <Route path="/charts" component={Charts}/>
            </div>
        )
    };
}

export default Menu;

