import React, {Component, Fragment} from "react";
import './style.css';
import InputButton from '../PopUp/InputButton/index';
import ButtonPopUp from "../ButtonPopUp";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getUserMail} from '../../AC/index';
import loginUser from "../../reducers/loginUser";

class PopUpForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isShowCancel: false,
            isButton: true,
            hits: null
        };
    }

    /* close pop-up */
    closeCancel = (e) => {
        e.preventDefault();
        this.setState({
            isShowCancel: false,
            isButton: true
        })
    };

    formOnChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value});
    };

    /* show pop-up */
    showLogin = (e) => {
        e.preventDefault();
        this.setState({
            isShowCancel: true,
            isButton: false,
        })
    };

    onSubmitCheck = (e) => {
        e.preventDefault();
    };

    render() {
        return (
            <Fragment>
                <ButtonPopUp showLogin={this.showLogin} isButton={this.state.isButton}/>
                {this.state.isShowCancel && <div className="form-style">
                    <a href="" className="cancel" onClick={this.closeCancel}> cancel</a>
                    <form action="#" onSubmit={this.onSubmitCheck}>
                        <div className="name">
                            <InputButton typeInput="text"
                                         valueInp={this.props.userName}
                                         name="Введите имя:"
                                         classInp="inp-name"
                                         changeInput={this.formOnChange}
                            />
                        </div>
                        <div className="password">
                            <InputButton typeInput="password"
                                         valueInp={this.props.userPassword}
                                         name="Введите пароль:"
                                         classInp="inp-password"
                                         changeInput={this.formOnChange}
                            />
                        </div>
                        <div className="mail">
                            <InputButton typeInput="email"
                                         valueInp={this.props.userMail}
                                         name="Введите почту:"
                                         classInp="inp-mail"
                                         changeInput={this.formOnChange}
                            />
                        </div>
                        <InputButton typeInput="submit" classInp="inp-button" onSubmitCheck={this.onSubmitCheck}/>
                    </form>
                </div>
                }
            </Fragment>
        )
    };
}

const mapStateToProps = state => ({
    activeUserMail: state.loginUser.activeUserMail,
    userName: state.loginUser.userName,
    userPassword: state.loginUser.userPassword,
    userMail: state.loginUser.userMail,
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            getUserMail
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(PopUpForm);


