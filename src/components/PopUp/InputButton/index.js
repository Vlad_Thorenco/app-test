import React, {Component} from "react";
import './style.css';

class InputButton extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="wraped-pop-up">
                <div className={this.props.classInp}>
                    <label htmlFor={this.props.name}>{this.props.name}</label>
                    <input type={this.props.typeInput}
                           id={this.props.name}
                           value={this.props.valueInp}
                           onChange={this.props.changeInput}
                           ref={this.props.refs}
                    />
                </div>
            </div>
        )
    };
}

export default InputButton;

