import React, {Component} from "react";
import './style.css';
import {
    Player, ControlBar, ReplayControl,
    ForwardControl, CurrentTimeDisplay,
    TimeDivider, PlaybackRateMenuButton, VolumeMenuButton
} from 'video-react';

class Video extends Component {
    static propTypes = {};

    constructor(props) {
        super(props);
        this.state = {
            loop: true,
            autoplay: true,
            controls: false,
        };
    }

    render() {
        return (
            <div className="Video">
                <Player poster="/assets/poster.png"
                        loop={this.state.loop}
                        autoPlay>
                    <source src="http://peach.themazzone.com/durian/movies/sintel-1024-surround.mp4"/>
                    <source src="http://mirrorblender.top-ix.org/movies/sintel-1024-surround.mp4"/>

                    <ControlBar autoHide={true}>
                        <ReplayControl seconds={10} order={1.1}/>
                        <ForwardControl seconds={30} order={1.2}/>
                        <CurrentTimeDisplay order={4.1}/>
                        <TimeDivider order={4.2}/>
                        <PlaybackRateMenuButton
                            rates={[5, 2, 1, 0.5, 0.1]}
                            order={7.1}
                        />
                        <VolumeMenuButton disabled/>
                    </ControlBar>
                </Player>
            </div>
        )
    };
}

export default Video;

