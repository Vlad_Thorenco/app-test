import React, {Component, Fragment} from "react";
import './style.css';

class ButtonPopUp extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Fragment>
                {
                    this.props.isButton && <div className="wrap-pop-up">
                        <a href="" className="btn-pop-up" onClick={this.props.showLogin}>show pop-up</a>
                    </div>
                }
            </Fragment>
        )
    };
}

export default ButtonPopUp;

