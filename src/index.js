import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from "react-router-dom";
import {Provider} from 'react-redux';
import store from './store/index';
import './index.css';
import App from './components/App';

const app = <BrowserRouter>
                <Provider store={store}>
                    <App/>
                </Provider>
            </BrowserRouter>;

ReactDOM.render(app, document.getElementById('root'));

