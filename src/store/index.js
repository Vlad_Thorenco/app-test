import {createStore, applyMiddleware, compose} from 'redux';
import RootReducer from '../reducers/index';
import thunk from 'redux-thunk';

// Метод redux который создает Store, вызывае 1 раз при анациализации
// reducer - аргумент

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        }) : compose;

const enhancer = composeEnhancers(
    applyMiddleware(thunk),
    // other store enhancers if any
);
const store = createStore(RootReducer, enhancer);

export default store;
